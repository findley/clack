import {AbstractPlayer} from 'common/player/abstractPlayer';
import {EventEmitter} from 'events';
import * as Clack from 'common/clack.interfaces';

export class Hub extends EventEmitter {
    protected _players: Array<AbstractPlayer>

    constructor() {
        super();
        this._players = [];
    }

    public broadcast(message: string, exclude: AbstractPlayer = null) {
        for (let player of this._players) {
            if (exclude && exclude == player) continue;
            player.send(message);
        }
    }

    public broadcastJSON(message: any, exclude: AbstractPlayer = null) {
        const m = JSON.stringify(message);
        this.broadcast(m, exclude);
    }

    public addPlayer(player: AbstractPlayer) {
        this._players.push(player);
        this.emit('player-added', player);

        player.on('message', (data: any, flags: {binary: boolean}) => {
            let message = JSON.parse(data) as Clack.Message;
            this.emit('player-message', player, message);
        });

        player.once('close', () => {
            this.removePlayer(player);
        });
    }

    public close() {
        for (let i = this._players.length-1; i >= 0; i--) {
            this._players[i].close();
        }
    }

    public removePlayer(player: AbstractPlayer) {
        for (let i in this._players) {
            if (this._players[i] == player) {
                this._players.splice(parseInt(i), 1);
                this.emit('player-left', player);
                break;
            }
        }
    }

    public getPlayerCount(): number {
        return this._players.length;
    }

    get players(): Array<AbstractPlayer> {
        return this._players;
    }
}
