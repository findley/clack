export interface Racer {
    name: string;
    progress?: number;
    wpm?: number;
    id: number;
    place?: number;
    connected?: boolean;
    ready?: boolean;
}

export interface Race {
    state: GameState;
    racers: Array<Racer>;
}

export interface Quote {
    content?: string;
    author?: string;
}

export enum GameState {
    Waiting, Loading, Starting, Started, Finished, Dropped
}

export interface Message {
    messageType: MessageType;
}

export enum MessageType {
    sRaceJoin,
    sRaceUpdate,
    sRoomJoin,
    sRoomStatus,

    cRacerUpdate,
    cLeaveRace,
    cPlayerReady,
    cStartRace,
    cEndRace,
    cTransferHost,
    cKickPlayer,

    gRoomChat
}

export interface RaceJoinMessage extends Message {
    raceId: number;
    race: Race;
    quote: Quote;
    startsIn: number;
    you?: number;
}

export interface RaceUpdateMessage extends Message {
    raceId: number;
    race: Race
}

export interface RacerUpdateMessage extends Message {
    progress: number;
}

export interface LeaveRaceCommand extends Message {

}

export interface RoomJoinMessage extends Message {
    roomId: string;
    you: number;
    host: number;
    players: Array<Racer>;
    inGame: boolean;
}

export interface ChatMessage extends Message {
    sender: string;
    message: string;
    system?: boolean;
}

export interface PlayerReadyMessage extends Message {

}

export interface StartRaceMessage extends Message {
}

export interface RoomStatusMessage extends Message {
    players: Array<Racer>;
    host: number;
    inGame: boolean;
}

export interface EndRaceMessage extends Message {
}

export interface TransferHostMessage extends Message {
    to: number;
}

export interface KickPlayerMessage extends Message {
    who: number;
}
