import * as WebSocket from 'ws';
import {IWebSocketEvent} from 'types';
import {EventEmitter} from 'events';

export abstract class AbstractPlayer extends EventEmitter {
    public name: string;
    public _progress: number;
    public wpm: number;
    public id: number;
    public connected: boolean = true;
    public place: number;
    public completedTime: Date;
    public ready: boolean = false;

    constructor() {
        super();
        this.progress = 0;
        this.wpm = 0;
        this.name = "Guest";
        this.id = this.getRandomId();
    }

    public abstract close();

    public abstract send(message: string);

    public sendJSON(message: any) {
        const m = JSON.stringify(message);
        this.send(m);
    }

    public reset() {
        this._progress = 0;
        this.wpm = 0;
        this.place = 0;
        this.completedTime = null;
        this.ready = false;
    }

    get progress(): number {
        return this._progress;
    }

    set progress(progress: number) {
        this._progress = progress;
        if (this.completedTime == null && this._progress >= 1) {
            this.completedTime = new Date();
        }
    }

    protected getRandomId(): number {
        return Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
    }
}
