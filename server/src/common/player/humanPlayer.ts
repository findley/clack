import * as WebSocket from 'ws';
import {IWebSocketEvent} from 'types';
import {EventEmitter} from 'events';
import {AbstractPlayer} from 'common/player/abstractPlayer';

export class HumanPlayer extends AbstractPlayer {
    public ws: WebSocket;

    constructor() {
        super();
    }

    set websocket(ws: WebSocket) {
        this.ws = ws;

        ws.on('close', () => {
            this.connected = false;
            this.emit('close');
        });

        ws.on('message', (msg, flags) => {
            this.emit('message', msg, flags);
        });
    }

    public send(message: string) {
        if (this.ws.readyState == WebSocket.OPEN) {
            this.ws.send(message);
        }
    }

    public close() {
        this.ws.close();
    }
}
