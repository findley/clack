import {AbstractPlayer} from 'common/player/abstractPlayer';
import * as Clack from 'common/clack.interfaces';

let nameList = [
    'Renea',
    'Deetta',
    'Ira',
    'Jamaal',
    'Shala',
    'Aurelio',
    'Ricardo',
    'Melvina',
    'Edison',
    'Dianne',
    'Bambi',
    'Natashia',
    'Chantay',
    'Renee',
    'Darryl',
    'Billy',
    'Zandra',
    'Del',
    'Cordie',
    'Aja',
    'Talisha',
    'Farah',
    'Lianne',
    'Terrence',
    'Ivan',
    'Raisa',
    'Flora',
    'Dorinda',
    'Kazuko',
    'Dona',
    'Bernadette',
    'Janie',
    'Angelita',
    'Shelli',
    'Zoe',
    'Christina',
    'Jeni',
    'Troy',
    'Molly',
    'Danilo',
    'Rosio',
    'Elizebeth',
    'Karoline',
    'Rea',
    'Bettye',
    'Cathern',
    'Floyd',
    'Beulah',
    'Rowena',
    'Lory',
	'Knockblocks',
	'DreamerGamers',
	'gnaskoow',
	'GoodGamerWellPlayer',
	'The_Last_Nephilim',
	'deeplow1',
	'anon69',
	'Fabinow',
	'El DaV',
	'ollyelche',
	'Biathuast',
	'Vincentgagnon98',
	'St0nedMonk',
	'Ranbog',
	'Kuitti',
	'itschickentime',
	'Noizi ito',
	'Dinojan',
	'Kingleoii',
	'Real devil z',
	'Gastlyguy',
	'luhantrash',
	'h4wken',
	'must be protein',
	'enlendo',
	'Reezy',
	'WindMage',
	'Kevincanobarrueta',
	'Sway',
	'Morph Kogan',
	'TankGuy',
	'Racer',
	'imustnotafk',
	'effeffess',
	'Warpitus',
	'Thr33Wishes',
	'BoomShaqaLaqa',
	'orphen369',
	'Rimrecker'
];

export class AiPlayer extends AbstractPlayer {
    private aiwpm: number;
    private raceString: string;
    private aiProgress: number = 0;

    constructor() {
        super();
        // this.name = "Computer";
        this.name = this.getRandomName();
        this.aiwpm = this.getRandomInt(40, 100);
    }

    public send(message: string) {
        let m: Clack.Message = JSON.parse(message);
        if (m.messageType == Clack.MessageType.sRaceJoin) {
            let joinMessage = m as Clack.RaceJoinMessage;
            this.raceString = joinMessage.quote.content;
            setTimeout(this.play.bind(this), joinMessage.startsIn);
        }
    }

    public close() {
        this.emit('close');
    }

    public getWpm(): number {
        return this.aiwpm;
    }

    private play() {
        let seconds = 0;
        let words = this.raceString.length / 5;
        let t = setInterval(() => {
            this.varyWpm();
            seconds++;
            this.aiProgress += (this.aiwpm / 60) / words;
            if (this.aiProgress > 0.99) {
                this.emit('message', JSON.stringify({messageType: Clack.MessageType.cRacerUpdate, progress: 1}));
                clearInterval(t);
            } else {
                this.emit('message', JSON.stringify({messageType: Clack.MessageType.cRacerUpdate, progress: this.aiProgress}));
            }
        }, 1000);
    }

    private getRandomName(): string {
        if (this.odds(1,3)) {
            let index = this.clamp(this.getRandomInt(0, nameList.length), 0, nameList.length - 1);
            return nameList[index];
        } else {
            return 'Guest';
        }
    }

    private varyWpm() {
        if (this.odds(1, 3)) {
            let magnitude = this.getRandomInt(1,3) * 0.1;
            let change = this.aiwpm * magnitude;
            if (this.odds(1,2)) {
                this.aiwpm = Math.floor(this.clamp(this.aiwpm + change, 20, 120));
            } else {
                this.aiwpm = Math.floor(this.clamp(this.aiwpm - change, 20, 120));
            }
        }
    }

    private clamp(n: number, min: number, max: number): number {
        if (n < min) return min;
        if (n > max) return max;
        return n;
    }

    private getRandomInt(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    private odds(x: number, y: number): boolean {
        if (x >= y) return true;
        let n = this.getRandomInt(1, y);
        return n <= x;
    }
}
