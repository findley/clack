import * as WebSocket from 'ws';

export interface IWebSocketEvent {
    data: any,
    type: string,
    target: WebSocket
}
