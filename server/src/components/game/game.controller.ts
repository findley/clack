import 'reflect-metadata';
import {injectable, inject} from 'inversify';
import {container} from 'ioc';

import * as Restify from 'restify';
import * as WebSocket from 'ws';
import * as url from 'url';
import * as querystring from 'querystring';
import {Game} from 'components/game/game';
import {Hub} from 'common/hub';
import {GameState} from 'common/clack.interfaces';
import {HumanPlayer} from 'common/player/humanPlayer';
import {AiPlayer} from 'common/player/aiPlayer';
import {QuoteService} from 'components/quote/quote.service';
import {IServerConfiguration} from 'configuration';
import * as Clack from 'common/clack.interfaces';

@injectable()
export class GameController {
    private static readonly GAME_SIZE = 4;
    private loadingGame: Game;
    private activeGames: Map<Game,boolean>;
    private playersConnected: number = 0;

    constructor(
        @inject('config') private config: IServerConfiguration,
        @inject(QuoteService) private quoteService: QuoteService,
        @inject('log') private log
    ) {
        this.activeGames = new Map<Game,boolean>();
    }

    public start() {
        this.loadingGame = this.getNewGame();
        this.injectAiPlayers();
    }

    public newConnection(ws: WebSocket) {
        this.playersConnected++;
        let params = querystring.parse(url.parse(ws.upgradeReq.url).query);

        let player = new HumanPlayer();
        if (params.name) {
            player.name = params.name;
        }
        player.websocket = ws;

        this.log.info({websocketAudit: 'game-connect', playerId: player.id, connectedPlayers: this.playersConnected}, 'Player connected to game.');
        ws.once('close', () => {
            this.playersConnected--;
            this.log.info({websocketAudit: 'game-disconnect', playerId: player.id, connectedPlayers: this.playersConnected}, 'Player disconnected from game.');
        });

        if (!this.loadingGame.acceptingPlayers()) {
            this.loadingGame = this.getNewGame();
        }
        this.loadingGame.hub.addPlayer(player);
        this.loadingGame.addPlayer(player);
    }

    private getNewGame() {
        let quote = this.quoteService.getRandomQuote();
        let game = new Game(new Hub(), quote, GameController.GAME_SIZE);
        game.once('start', g => {
            this.activeGames.set(g, true);
            this.log.info({websocketAudit: 'game-open', gameId: game.gameId, openGames: this.activeGames.size}, 'New game opened.');
        });
        game.once('end', g => {
            setTimeout(() => {
                g.hub.close();
            });
            this.activeGames.delete(g);
            this.log.info({websocketAudit: 'game-close', gameId: game.gameId, openGames: this.activeGames.size}, 'Game closed.');
        });
        return game;
    }

    private injectAiPlayers() {
        setInterval(() => {
            if (this.loadingGame.acceptingPlayers() && this.loadingGame.getPlayerCount() > 0) {
                let ai = new AiPlayer();
                this.loadingGame.hub.addPlayer(ai);
                this.loadingGame.addPlayer(ai);
                this.log.info('Injecting AI player with wpm: ', ai.getWpm());
            }
        }, 7000);
    }
}

container.bind(GameController).toSelf();
