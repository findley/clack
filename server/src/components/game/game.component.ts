import 'reflect-metadata';
import {injectable, inject} from 'inversify';
import {container} from 'ioc';

import * as Restify from 'restify';
import * as WebSocket from 'ws';
import {IComponent} from 'components/component';
import {GameController} from 'components/game/game.controller';

@injectable()
export class GameComponent implements IComponent {
    private server: WebSocket.Server;

    constructor(@inject(GameController) private gameController: GameController, @inject("log") private log) {}

    public attach(server: Restify.Server, basePath: string) {
        let route = basePath + "/game";
        this.gameController.start();
        this.server = new WebSocket.Server({server: server, path: route});

        this.server.on('connection', this.gameController.newConnection.bind(this.gameController));

        this.log.info('Accepting websocket connections at %s', route);
    }
}

container.bind(GameComponent).toSelf();
