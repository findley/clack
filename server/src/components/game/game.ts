import {AbstractPlayer} from 'common/player/abstractPlayer';
import {HumanPlayer} from 'common/player/humanPlayer';
import {Hub} from 'common/hub';
import * as Clack from 'common/clack.interfaces';
import {EventEmitter} from 'events';

export class Game extends EventEmitter {
    private static readonly GAME_JOIN_CUTOFF = 3500;
    private static readonly TICKS_PER_SECOND = 3;
    private _state: Clack.GameState;
    private initialTime: Date;
    private launchTime: Date;
    private gameSize: number;
    private launchTimer: NodeJS.Timer;
    private updateInterval: NodeJS.Timer;
    private _lastUpdateMessage: string;
    private _quote: Clack.Quote;
    private _hub: Hub;
    private _players: Array<AbstractPlayer>;
    private _gameStartDelay = 10000;
    private _gameId: number;

    constructor(hub: Hub, quote: Clack.Quote, gameSize: number = 4) {
        super();
        this._hub = hub;
        this._state = Clack.GameState.Loading;
        this.gameSize = gameSize;
        this._quote = quote;
        this._players = [];

        let leaveHandler = this.playerLeft.bind(this);
        let messageHandler = this.playerMessage.bind(this);
        hub.on('player-left', leaveHandler);
        hub.on('player-message', messageHandler);

        this.once('end', () => {
            hub.removeListener('player-left', leaveHandler);
            hub.removeListener('player-message', messageHandler);
        });

        this._gameId = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
    }

    public getPlayerCount(): number {
        return this._players.length;
    }

    get state(): Clack.GameState {
        return this._state;
    }

    get hub(): Hub {
        return this._hub;
    }

    get gameId(): number {
        return this._gameId;
    }

    set gameStartDelay(delay: number) {
        this._gameStartDelay = delay;
    }

    public acceptingPlayers(): boolean {
        if (this._state != Clack.GameState.Loading) return false;
        if (!this.initialTime) return true;
        if (this._gameStartDelay - (Date.now() - this.initialTime.getTime()) < Game.GAME_JOIN_CUTOFF) return false;
        return true;
    }

    public addPlayer(player: AbstractPlayer) {
        this.initPlayer(player);
        let message = this.makeRaceUpdateMessage();
        this._hub.broadcast(JSON.stringify(message), player);
        player.send(JSON.stringify(this.makeRaceJoinMessage(player)));
    }

    public batchAddPlayers(players: Array<AbstractPlayer>) {
        for (let player of players) {
            this.initPlayer(player);
            player.send(JSON.stringify(this.makeRaceJoinMessage(player)));
        }
        this._hub.broadcast(JSON.stringify(this.makeRaceUpdateMessage()));
    }

    public batchAddSpectators(players: Array<AbstractPlayer>) {
        for (let player of players) {
            player.send(JSON.stringify(this.makeRaceJoinMessage()));
        }
    }

    public forceEnd()
    {
        this._state = Clack.GameState.Finished;
    }

    private initPlayer(player: AbstractPlayer) {
        player.progress = 0;
        this._players.push(player);
        if (this._players.length == 1) {
            this.initialTime = new Date();
            this.launchTimer = setTimeout(() => {
                this.start();
            }, this._gameStartDelay);
        }
        if (this._players.length >= this.gameSize) {
            this._state = Clack.GameState.Starting;
        }
    }

    private start() {
        if (this._state != Clack.GameState.Loading && this._state != Clack.GameState.Starting) return;
        this._state = Clack.GameState.Started;
        this.emit('start', this);
        this.launchTime = new Date();
        console.log('Set launch time to:', this.launchTime);
        this._state = Clack.GameState.Started;
        this._hub.broadcastJSON(this.makeRaceUpdateMessage());
        this.updateInterval = setInterval(() => {
            if (this.isGameDone()) {
                clearInterval(this.updateInterval);
                this.emit('end', this);
                this._state = Clack.GameState.Finished;
            }
            let updateMessage = JSON.stringify(this.makeRaceUpdateMessage());
            if (this._lastUpdateMessage != null && updateMessage == this._lastUpdateMessage) {
                return;
            }
            this._hub.broadcast(updateMessage);
            this._lastUpdateMessage = updateMessage;
        }, (1 / Game.TICKS_PER_SECOND) * 1000);
    }

    private isGameDone() {
        if (this._state == Clack.GameState.Finished) return true;
        let hasHuman = false;
        let isDone = true;
        for (let player of this._players) {
            if (player instanceof HumanPlayer && player.connected) {
                hasHuman = true;
            }
            if (player.connected && player.progress < 1) {
                isDone = false;
            }
        }
        return isDone || !hasHuman;
    }

    protected playerLeft(player: AbstractPlayer) {
        if (this._state != Clack.GameState.Loading && this._players.length == 0) {
            this._state = Clack.GameState.Dropped;
            clearTimeout(this.launchTimer);
        }
    }

    protected playerMessage(player: AbstractPlayer, message: Clack.Message) {
        switch (message.messageType) {
            case Clack.MessageType.cRacerUpdate:
                let racerUpdate = message as Clack.RacerUpdateMessage;
                if (racerUpdate.progress > player.progress) {
                    player.progress = racerUpdate.progress;
                    player.wpm = this.calculateWpm(racerUpdate.progress);
                }
                break;
            case Clack.MessageType.cLeaveRace:
                player.close();
                break;
        }
    }

    private makeRaceJoinMessage(player: AbstractPlayer = null): Clack.RaceJoinMessage {
        let race = this.makeRaceMessage();

        let message: Clack.RaceJoinMessage = {
            messageType: Clack.MessageType.sRaceJoin,
            raceId: this._gameId,
            race: race,
            quote: this._quote,
            startsIn: this.initialTime.getTime() + this._gameStartDelay - Date.now(),
        };

        if (player) {
            message.you = player.id;
        }

        return message;
    }

    private makeRaceUpdateMessage(): Clack.RaceUpdateMessage {
        let race = this.makeRaceMessage();

        let message: Clack.RaceUpdateMessage = {
            messageType: Clack.MessageType.sRaceUpdate,
            raceId: this._gameId,
            race: race
        };

        return message;
    }

    private makeRaceMessage(): Clack.Race {
        let racers: Array<Clack.Racer> = [];
        let players = this._players;
        this.updatePlaces();
        for (let i = 0; i < players.length; i++) {
            racers.push({
                name: players[i].name,
                progress: players[i].progress,
                wpm: players[i].wpm,
                id: players[i].id,
                place: players[i].place,
                connected: players[i].connected
            });
        }
        return {
            state: this._state,
            racers: racers
        }
    }

    private updatePlaces() {
        let players = this._players;
        for (let player of players) {
            if (!player.completedTime) continue;
            player.place = players.reduce((rank,p) => {
                return (p.completedTime && p.completedTime < player.completedTime) ? rank + 1 : rank;
            }, 1);
        }
    }

    private calculateWpm(progress: number) {
        let totalWords = this._quote.content.length / 5;
        let words = progress * totalWords;
        let minutes = ( (new Date()).getTime() - this.launchTime.getTime() ) / (1000 * 60);
        return words / minutes;
    }
}
