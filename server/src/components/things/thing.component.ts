import 'reflect-metadata';
import {injectable, inject} from 'inversify';
import {container} from 'ioc';

import * as Restify from 'restify';
import {ThingController} from 'components/things/thing.controller';
import {IServerConfiguration} from "configuration";
import {IComponent} from 'components/component';

@injectable()
export class ThingComponent implements IComponent {
    constructor(@inject(ThingController) private thingController: ThingController) {}

    public attach(server: Restify.Server, basePath: string) {
        server.get(basePath + '/things', this.thingController.get.bind(this.thingController));
    }
}

container.bind(ThingComponent).toSelf();
