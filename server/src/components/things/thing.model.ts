export interface IThing {
    id: number;
    name: string;
    thingThing: string;
}

export class Thing implements IThing {
    id: number;
    name: string;
    thingThing: string;
}
