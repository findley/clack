import 'reflect-metadata';
import {injectable, inject} from 'inversify';
import {container} from 'ioc';

import {IThing, Thing} from 'components/things/thing.model';

@injectable()
export class ThingService {
    public getThing() {
        let thing = new Thing();
        thing.name = 'lala';
        return thing;
    }
}

container.bind(ThingService).toSelf();
