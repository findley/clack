import 'reflect-metadata';
import {injectable, inject} from 'inversify';
import {container} from 'ioc';

import * as Restify from 'restify';
import {ThingService} from 'components/things/thing.service';
import {IServerConfiguration} from "configuration";

@injectable()
export class ThingController {

    constructor(
        @inject('config') private config: IServerConfiguration,
        @inject(ThingService) private thingService: ThingService
    ) {}

    public get(request: Restify.Request, response: Restify.Response) {
        let result = this.thingService.getThing();
        response.send(result);
    }
}

container.bind(ThingController).toSelf();
