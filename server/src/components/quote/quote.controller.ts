import 'reflect-metadata';
import {injectable, inject} from 'inversify';
import {container} from 'ioc';

import * as Restify from 'restify';
import {QuoteService} from 'components/quote/quote.service';
import {IServerConfiguration} from "configuration";

@injectable()
export class QuoteController {

    constructor(
        @inject('config') private config: IServerConfiguration,
        @inject(QuoteService) private quoteService: QuoteService
    ) {}

    public get(request: Restify.Request, response: Restify.Response, next: Restify.Next) {
        request.log.info('Serving quote');
        let result = this.quoteService.getRandomQuote();
        response.send(result);
        return next();
    }
}

container.bind(QuoteController).toSelf();
