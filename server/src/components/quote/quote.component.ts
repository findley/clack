import 'reflect-metadata';
import {injectable, inject} from 'inversify';
import {container} from 'ioc';

import * as Restify from 'restify';
import {QuoteController} from 'components/quote/quote.controller';
import {IServerConfiguration} from "configuration";
import {IComponent} from 'components/component';

@injectable()
export class QuoteComponent implements IComponent {
    constructor(@inject(QuoteController) private quoteController: QuoteController) {}

    public attach(server: Restify.Server, basePath: string) {
        server.get(basePath + '/quotes', this.quoteController.get.bind(this.quoteController));
    }
}

container.bind(QuoteComponent).toSelf();
