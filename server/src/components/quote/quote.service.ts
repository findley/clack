import 'reflect-metadata';
import {injectable, inject} from 'inversify';
import {container} from 'ioc';

import * as fs from 'fs';
import * as Clack from 'common/clack.interfaces';
import {IServerConfiguration} from 'configuration';

@injectable()
export class QuoteService {
    private _quotes: Array<Clack.Quote>;
    private _loaded: boolean = false;

    constructor(
        @inject('config') private config: IServerConfiguration,
        @inject('log') private log
    ) {}

    public getRandomQuote(): Clack.Quote {
        this.load();
        return this._quotes[this.getRandomInt(0, this._quotes.length)]
    }

    private load() {
        if (this._loaded) return;
        let quoteString = fs.readFileSync(this.config.quoteFile, 'utf8');
        this._quotes = JSON.parse(quoteString);
        this.log.info('Loaded %d quotes from: %s', this._quotes.length, this.config.quoteFile);
        this._loaded = true;
    }

    private getRandomInt(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min)) + min;
    }
}

container.bind(QuoteService).toSelf().inSingletonScope();
