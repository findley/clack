import * as shortid from 'shortid';
import {EventEmitter} from 'events';
import {AbstractPlayer} from 'common/player/abstractPlayer';
import {Hub} from 'common/hub';
import {Game} from 'components/game/game';
import * as Clack from 'common/clack.interfaces';
import {container} from 'ioc';
import {QuoteService} from 'components/quote/quote.service'

export class Room extends EventEmitter {
    private _id: string;
    private _host: AbstractPlayer;
    private _hub: Hub;
    private _game: Game;
    private _quoteService: QuoteService

    constructor(hub: Hub) {
        super();
        this._hub = hub;
        this._id = shortid.generate();
        this._quoteService = container.get(QuoteService);

        let addHandler = this.playerAdded.bind(this);
        let leaveHandler = this.playerLeft.bind(this);
        let messageHandler = this.playerMessage.bind(this);
        hub.on('player-added', addHandler);
        hub.on('player-left', leaveHandler);
        hub.on('player-message', messageHandler);

        this.once('end', () => {
            hub.removeListener('player-added', addHandler);
            hub.removeListener('player-left', leaveHandler);
            hub.removeListener('player-message', messageHandler);
        });
    }

    get id(): string {
        return this._id;
    }

    get hub(): Hub {
        return this._hub;
    }

    protected playerAdded(player: AbstractPlayer) {
        if (this._hub.players.length == 1) {
            this._host = player;
        }
        player.sendJSON(this.makeRoomJoinMessage(player));
        this._hub.broadcastJSON(this.makeRoomStatusMessage(), player);
        this.sendSystemChatMessage(player.name + ' has joined the room.');
    }

    protected playerLeft(player: AbstractPlayer) {
        if (this._hub.players.length == 0) {
            this.emit('end', this);
        } else {
            if (player.id == this._host.id) {
                this._host = this._hub.players[0];
            }
            this._hub.broadcastJSON(this.makeRoomStatusMessage());
            this.sendSystemChatMessage(player.name + ' has left the room.');
        }
    }

    protected playerMessage(player: AbstractPlayer, message: Clack.Message) {
        switch (message.messageType) {
            case Clack.MessageType.gRoomChat:
                this.handleChatMessage(player, message);
                break;
            case Clack.MessageType.cPlayerReady:
                this.handlePlayerReadyMessage(player, message);
                break;
            case Clack.MessageType.cStartRace:
                this.handleStartRaceMessage(player, message);
                break;
            case Clack.MessageType.cTransferHost:
                this.handleTransferHost(player, message);
                break;
            case Clack.MessageType.cEndRace:
                this.handleEndRace(player, message);
                break;
            case Clack.MessageType.cKickPlayer:
                this.handleKickPlayer(player, message);
                break;
        }
    }

    protected handleChatMessage(player: AbstractPlayer, message: Clack.Message) {
        let roomChat = message as Clack.ChatMessage;
        if (roomChat.message == '') return;
        roomChat.sender = player.name;
        this._hub.broadcastJSON(roomChat);
    }

    protected handlePlayerReadyMessage(player: AbstractPlayer, message: Clack.Message) {
        let playerReady = message as Clack.PlayerReadyMessage;
        player.ready = true;
        this._hub.broadcastJSON(this.makeRoomStatusMessage(), player)
    }

    protected handleStartRaceMessage(player: AbstractPlayer, message: Clack.Message) {
        if (player != this._host) return;
        if (this._game != null) return;
        player.ready = true;
        this._game = new Game(this._hub, this._quoteService.getRandomQuote());
        this._game.gameStartDelay = 5000;

        let readyPlayers = [];
        let spectators = [];
        for (let player of this._hub.players) {
            if (player.ready) {
                readyPlayers.push(player);
            } else {
                spectators.push(player);
            }
            player.reset();
        }

        this._game.batchAddPlayers(readyPlayers);
        this._game.batchAddSpectators(spectators);

        this._hub.broadcastJSON(this.makeRoomStatusMessage());
        this._game.once('end', () => {
            this._game = null;
            this._hub.broadcastJSON(this.makeRoomStatusMessage());
        });
    }

    protected handleTransferHost(player: AbstractPlayer, message: Clack.Message) {
        if (player != this._host) return;
        let transferMessage = message as Clack.TransferHostMessage;
        for (let player of this._hub.players) {
            if (player.id == transferMessage.to) {
                this._host = player;
                this._hub.broadcastJSON(this.makeRoomStatusMessage());
                break;
            }
        }
    }

    protected handleEndRace(player: AbstractPlayer, message: Clack.Message) {
        if (player != this._host) return;
        if (this._game == null) return;
        this._game.forceEnd();
    }

    protected handleKickPlayer(player: AbstractPlayer, message: Clack.Message) {
        if (player != this._host) return;
        let kickMessage = message as Clack.KickPlayerMessage;

    }

    protected makeRoomJoinMessage(player: AbstractPlayer): Clack.RoomJoinMessage {
        return {
            messageType: Clack.MessageType.sRoomJoin,
            roomId: this._id,
            you: player.id,
            host: this._host.id,
            players: this.makeRoomPlayerList(),
            inGame: this._game != null
        };
    }

    protected makeRoomStatusMessage(): Clack.RoomStatusMessage {
        return {
            messageType: Clack.MessageType.sRoomStatus,
            host: this._host.id,
            players: this.makeRoomPlayerList(),
            inGame: this._game != null
        };
    }

    protected makeRoomPlayerList(): Array<Clack.Racer> {
        let roomPlayers: Array<Clack.Racer> = [];
        let players = this._hub.players;
        for(let i = 0; i < players.length; i++) {
            roomPlayers.push({
                name: players[i].name,
                id: players[i].id,
                ready: players[i].ready,
                connected: players[i].connected,
            });
        }
        return roomPlayers;
    }

    protected sendSystemChatMessage(message: string) {
        let msg: Clack.ChatMessage = {
            messageType: Clack.MessageType.gRoomChat,
            sender: '',
            message: message,
            system: true
        }
        this._hub.broadcastJSON(msg);
    }
}
