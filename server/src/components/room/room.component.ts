import 'reflect-metadata';
import {injectable, inject} from 'inversify';
import {container} from 'ioc';

import * as Restify from 'restify';
import * as WebSocket from 'ws';
import {IComponent} from 'components/component';
import {RoomController} from 'components/room/room.controller';

@injectable()
export class RoomComponent implements IComponent {
    private server: WebSocket.Server;

    constructor(
        @inject(RoomController) private roomController: RoomController,
        @inject("log") private log
    ) {}

    public attach(server: Restify.Server, basePath: string) {
        this.server = new WebSocket.Server({server: server, path: basePath + "/room"});

        this.server.on('connection', this.roomController.newConnection.bind(this.roomController));

        this.log.info('Accepting websocket connections at /room');
    }
}

container.bind(RoomComponent).toSelf();
