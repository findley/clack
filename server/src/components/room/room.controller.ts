import 'reflect-metadata';
import {injectable, inject} from 'inversify';
import {container} from 'ioc';

import * as Restify from 'restify';
import * as WebSocket from 'ws';
import * as url from 'url';
import * as querystring from 'querystring';
import {Room} from 'components/room/room';
import {Hub} from 'common/hub';
import {HumanPlayer} from 'common/player/humanPlayer';
import {IServerConfiguration} from 'configuration';
import * as Clack from 'common/clack.interfaces';

@injectable()
export class RoomController {
    private activeRooms: Map<string,Room>;
    private playersConnected: number = 0;

    constructor(
        @inject('config') private config: IServerConfiguration,
        @inject('log') private log
    ) {
        this.activeRooms = new Map<string,Room>();
    }

    public newConnection(ws: WebSocket) {
        this.playersConnected++;

        let params = querystring.parse(url.parse(ws.upgradeReq.url).query);
        let player = new HumanPlayer();
        if (params.name) {
            player.name = params.name;
        }
        player.websocket = ws;

        this.log.info({websocketAudit: 'room-connect', playerId: player.id, connectedPlayers: this.playersConnected}, 'Player connected to room.');
        ws.once('close', () => {
            this.playersConnected--;
            this.log.info({websocketAudit: 'room-disconnect', playerId: player.id, connectedPlayers: this.playersConnected}, 'Player disconnected from room.');
        });

        if (params.roomId) {
            if (this.activeRooms.has(params.roomId)) {
                this.activeRooms.get(params.roomId).hub.addPlayer(player);
            } else {
                this.openNewRoom().hub.addPlayer(player);
            }
        } else {
            this.openNewRoom().hub.addPlayer(player);
        }
    }

    private openNewRoom(): Room {
        let room = new Room(new Hub());
        this.activeRooms.set(room.id, room);
        this.log.info({websocketAudit: 'room-open', roomId: room.id, openRooms: this.activeRooms.size}, 'New room opened.');
        room.once('end', r => {
            r.hub.close();
            this.activeRooms.delete(room.id);
            this.log.info({websocketAudit: 'room-close', roomId: room.id, openRooms: this.activeRooms.size}, 'Room closed.');
        });

        return room;
    }
}

container.bind(RoomController).toSelf();
