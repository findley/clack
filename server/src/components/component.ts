import * as Restify from "restify";

export interface IComponent {
    attach(server: Restify.Server, basePath: string);
}
