export interface IServerConfiguration {
    port: number;
    quoteFile: string;
    basePath: string;
}
