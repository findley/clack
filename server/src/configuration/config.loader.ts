import 'reflect-metadata';
import {injectable} from 'inversify';
import {container} from 'ioc';

import {IServerConfiguration} from 'configuration/config.interface';

@injectable()
export class ConfigLoader {
    private static DEFAULT_CONFIG: IServerConfiguration = {
        port: 3000,
        quoteFile: 'quotes.json',
        basePath: '/api'
    };

    public loadFromFile(filename: string): IServerConfiguration {
        return {
            port: 80,
            quoteFile: 'quotes.json',
            basePath: '/api'
        }
    }

    public loadDefault(): IServerConfiguration {
        return ConfigLoader.DEFAULT_CONFIG;
    }
}

container.bind(ConfigLoader).toSelf();
