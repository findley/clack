import 'reflect-metadata';
import {injectable, inject} from 'inversify';
import {container} from 'ioc';

import * as Restify from "restify";
import {IServerConfiguration} from "configuration";
import {IComponent} from 'components/component';
import {ThingComponent} from 'components/things';
import {QuoteComponent} from 'components/quote';
import {GameComponent} from 'components/game';
import {RoomComponent} from 'components/room';

@injectable()
export class Server {
    private server: Restify.Server;

    @inject(ThingComponent)
    private thingComponent: IComponent;

    @inject(GameComponent)
    private gameComponent: IComponent;

    @inject(QuoteComponent)
    private quoteComponent: IComponent;

    @inject(RoomComponent)
    private roomComponent: IComponent;

    private basePath: string;

    constructor(@inject("config") private config: IServerConfiguration, @inject("log") private log) {
        this.server = Restify.createServer({
            log: this.log
        });
        this.server.use(Restify.CORS());
        this.basePath = this.config.basePath;
    }

    public start() {
        this.setupRateLimit();
        this.attachComponents();
        this.server.listen(this.config.port, () => {
            this.log.info('Server running at %s', this.server.url);
        });
    }

    public setupRateLimit() {
        this.server.use(Restify.throttle({
            rate: 1,
            burst: 3,
            ip: true
        }));
    }

    private attachComponents() {
        this.thingComponent.attach(this.server, this.basePath);
        this.gameComponent.attach(this.server, this.basePath);
        this.quoteComponent.attach(this.server, this.basePath);
        this.roomComponent.attach(this.server, this.basePath);
    }
}

container.bind(Server).toSelf();
