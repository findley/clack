require('app-module-path').addPath(__dirname);

import * as Bunyan from "bunyan";
import {container} from 'ioc';
import {Server} from 'server';
import {ConfigLoader, IServerConfiguration} from "configuration";

/* Load configuration and bind it to IOC container */
let configLoader = container.get(ConfigLoader);
let config = configLoader.loadDefault();
container.bind<IServerConfiguration>("config").toConstantValue(config);

let log = Bunyan.createLogger({
    name: 'audit',
    stream: process.stdout
});
container.bind("log").toConstantValue(log);

/* Start server*/
let server = container.get(Server);
server.start();
