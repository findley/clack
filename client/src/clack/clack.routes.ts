import {Routes} from '@angular/router';
import {GameComponent} from 'clack/game/game.component';
import {SoloComponent} from 'clack/solo/solo.component';
import {RoomComponent} from 'clack/room/room.component';

export const rootRouterConfig: Routes = [
  {path: '', redirectTo: 'game', pathMatch: 'full'},
  {path: 'game', component: GameComponent},
  {path: 'solo', component: SoloComponent},
  {path: 'room', component: RoomComponent}
];

