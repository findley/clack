import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import * as Clack from 'clack/shared/clack.interfaces';
import Room from 'clack/models/room.model';
import Connection from 'clack/models/connection.model';

@Injectable()
export class RoomService {

    constructor() {}

    public join(name: string, roomId: string = null): Room {
        let queryVars = [];
        if (name) {
            queryVars.push('name=' + name);
        }
        if (roomId) {
            queryVars.push('roomId=' + roomId);
        }
        let queryString = '';
        if (queryVars.length > 0) {
            queryString = '?' + queryVars.join('&');
        }

        let httpProto = window.location.protocol;
        let wsProto = 'ws:';
        if (httpProto == 'https:') {
            wsProto = 'wss:';
        }

        let connection = Connection.fromUrl(wsProto + '//' + window.location.host + API_URL + '/room' + queryString);
        return new Room(connection);
    }
}
