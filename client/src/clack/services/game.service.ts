import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import * as Clack from 'clack/shared/clack.interfaces';
import Game from 'clack/models/game.model';
import Connection from 'clack/models/connection.model';

@Injectable()
export class GameService {

    constructor(private http: Http) {}

    public queue(name: string): Game {
        let queryString = '';
        if (name) {
            queryString = '?name=' + name;
        }

        let httpProto = window.location.protocol;
        let wsProto = 'ws:';
        if (httpProto == 'https:') {
            wsProto = 'wss:';
        }

        let connection = Connection.fromUrl(wsProto + '//' + window.location.host + API_URL + '/game' + queryString);
        return new Game(connection);
    }

    public getQuote(): Observable<Clack.Quote> {
        return this.http.get(API_URL + '/quotes').map(this.extractQuote).catch(this.handleError);
    }

    private extractQuote(res: Response): Clack.Quote {
        return res.json();
    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}
