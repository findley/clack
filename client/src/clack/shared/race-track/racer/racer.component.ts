import {Component, Input} from '@angular/core';
import * as Clack from 'clack/shared/clack.interfaces';

@Component({
    selector: 'racer',
    templateUrl: './racer.component.html',
    styleUrls: ['./racer.component.css']
})
export class RacerComponent {
    @Input()
    racer: Clack.Racer;

    @Input()
    me: boolean;
}
