import {Component, Input} from '@angular/core';
import * as Clack from 'clack/shared/clack.interfaces';

@Component({
    selector: 'race-track',
    templateUrl: './race-track.component.html'
})
export class RaceTrackComponent {
    private _racers: Array<Clack.Racer> = [];

    private me: Clack.Racer;

    @Input()
    set racers(racers: Array<Clack.Racer>) {
        this._racers = [];

        if (!racers) return;

        for (let racer of racers) {
            if (racer.id == this.me.id) {
                this.me = racer;
            }
            this._racers.push(racer);
        }
    }

    @Input()
    set myProgress(progress: number) {
        if (this.me && progress) {
            this.me.progress = progress;
        }
    }

    @Input()
    set myId(id: number) {
        this.me.id = id;
        for (let i in this._racers) {
            if (this._racers[i].id == this.me.id) {
                this.me = this._racers[i];
            }
        }
    }

    constructor() {
        this.me = {
            id: 0,
            name: 'Guest',
            progress: 0,
            wpm: 0,
            place: null,
            connected: true
        }
    }
}
