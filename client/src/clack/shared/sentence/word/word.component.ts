import {Component, Input, ChangeDetectionStrategy} from '@angular/core';
import Word from 'clack/models/word.model';

@Component({
    selector: 'word',
    templateUrl: './word.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WordComponent {
    @Input()
    word: Word;
}
