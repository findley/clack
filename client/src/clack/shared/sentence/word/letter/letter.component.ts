import {Component, Input, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import Letter from 'clack/models/letter.model';
import {State} from 'clack/models/letter.model';

@Component({
    selector: 'letter',
    styleUrls: ['./letter.component.css'],
    templateUrl: './letter.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LetterComponent {
    _letter: Letter;

    constructor(private cd: ChangeDetectorRef) {}

    @Input()
    set letter(l: Letter) {
        this._letter = l;
        this._letter.subscribe(() => {
            this.cd.markForCheck();
        });
    }

    setClasses() {
        let classes = {
            'state-waiting': this._letter.state == State.Waiting,
            'state-active': this._letter.state == State.Active,
            'state-correct': this._letter.state == State.Correct,
            'state-incorrect': this._letter.state == State.Incorrect,
            'state-corrected': this._letter.state == State.Corrected
        };
        return classes;
    }
}
