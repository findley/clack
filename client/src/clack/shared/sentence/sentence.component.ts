import {Component, Input, Output, HostListener, EventEmitter, ElementRef, ChangeDetectionStrategy} from '@angular/core';
import Sentence from 'clack/models/sentence.model';
import {Progress} from 'clack/models/progress.interface';

export enum GameMode {
    Standard,
    NoMistake
}

@Component({
    selector: 'sentence',
    styleUrls: ['./sentence.component.css'],
    templateUrl: './sentence.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class SentenceComponent {
    @Input()
    set raceString(s: string) {
        this.setup(s);
    }

    @Input()
    public gameMode: GameMode;

    @Input()
    public enabled: boolean;

    @Output()
    done = new EventEmitter();

    @Output()
    progress = new EventEmitter<Progress>();

    /** The type string broken into an array of single character strings */
    sentence: Sentence;

    /** Indicates that the string has been typed */
    complete: boolean = false;

    private _keyCount: number = 0;

    private _componentTop: number = 0;
    private _componentHeight: number = 0;

    constructor(private _element: ElementRef) {
    }

    setup(s: string) {
        if (!s) return;

        this.sentence = new Sentence(s);
        this.sentence.words[0].letters[0].makeActive();
        this.complete = false;
        this._keyCount = 0;
        this.scrollElement(0, 500);
    }

    @HostListener('window:keydown', ['$event'])
    keyHandler(event: KeyboardEvent) {
        if (!this.enabled) return;
        if (event.key.length > 1 && event.keyCode != 8) return;
        if (event.altKey || event.ctrlKey || event.metaKey || event.keyCode == 16 || event.keyCode == 13 || this.complete) return;

        event.preventDefault();

        if (this._keyCount % 20 == 0) {
            this.updateMeasurements();
            let a_top = document.getElementsByClassName('state-active')[0].getBoundingClientRect().top;
            let a_offset = a_top - this._componentTop;
            if (a_offset > this._componentHeight - 80) {
                let newScroll = this._element.nativeElement.scrollTop + a_offset - 80;
                this.scrollElement(newScroll, 500);
            }
        }

        if (event.keyCode == 8) {
            if (this.gameMode == GameMode.Standard) {
                this.handleBackspace();
            }
            return;
        }
        this.handleRegularKey(event.key);
    }

    private handleBackspace() {
        this.sentence.previous();
        this.progress.emit(this.sentence.progress());
    }

    private handleRegularKey(key: string) {
        let word = this.sentence.current();
        let letter = word.current();

        this._keyCount++;

        if (key == letter.character) {
            letter.markCorrect();
        } else {
            if (this.gameMode == GameMode.Standard) {
                letter.markIncorrect();
            } else {
                letter.flashMistake();
            }
        }

        if (this.sentence.isAtEnd()) {
            if (this.gameMode == GameMode.Standard || key == letter.character) {
                this.complete = true;
                let progress = this.sentence.progress();
                progress.percentage = 1;
                this.progress.emit(progress);
                this.done.emit(this.sentence.result());
            }
        } else {
            if (this.gameMode == GameMode.Standard || key == letter.character) {
                this.progress.emit(this.sentence.progress());
                this.sentence.next();
            }
        }
    }

    private updateMeasurements() {
        let rect = this._element.nativeElement.getBoundingClientRect();
        this._componentTop = rect.top;
        this._componentHeight = rect.height;
    }

    private scrollElement(scrollTop: number, duration: number) {
        let e = this._element;
        let currentScrollTop = e.nativeElement.scrollTop;
        let diff = scrollTop - currentScrollTop;
        let cosParam = diff / 2;
        let scrollCount = 0;
        let oldTimestamp = performance.now();
        function step(newTimestamp) {
            scrollCount += Math.PI / (duration / (newTimestamp - oldTimestamp));
            if (scrollCount >= Math.PI) {
                e.nativeElement.scrollTop = scrollTop;
                return;
            };
            e.nativeElement.scrollTop = currentScrollTop + Math.round(cosParam * (Math.cos(scrollCount+Math.PI) + 1));
            oldTimestamp = newTimestamp;
            window.requestAnimationFrame(step);
        }
        window.requestAnimationFrame(step);
    }
}
