import {Component, Input, ElementRef} from '@angular/core';

@Component({
    selector: 'copyit',
    styleUrls: ['./copyit.component.css'],
    templateUrl: './copyit.component.html',
})
export class CopyitComponent {
    @Input()
    public copyText: string;

    @Input()
    public copyName: string;
}
