import {Component, Input, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';

@Component({
    selector: 'countdown',
    styleUrls: ['./countdown.component.css'],
    templateUrl: './countdown.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CountdownComponent {
    _countTime: number;

    constructor(private cd: ChangeDetectorRef) {}

    @Input()
    set time(ms: number) {
        if (!ms) return;
        this._countTime = ms;
        let t = setInterval(() => {
            this._countTime -= 1000;
            if (this._countTime <= 0) {
                clearInterval(t);
                this._countTime = 0;
                return;
            }
            if (this._countTime < 1000) {
                clearInterval(t);
                setTimeout(() => {
                    this._countTime = 0;
                    this.cd.markForCheck();
                }, this._countTime);
            }
            this.cd.markForCheck();
        }, 1000);
    }
}
