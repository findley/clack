import {Component, Input, Output, EventEmitter, ElementRef, AfterViewChecked, ViewChild} from '@angular/core';
import * as Clack from 'clack/shared/clack.interfaces';

@Component({
    selector: 'chat',
    styleUrls: ['./chat.component.css'],
    templateUrl: './chat.component.html',
})
export class ChatComponent implements AfterViewChecked {
    @Input()
    public history: Array<Clack.ChatMessage>

    @Output()
    public send = new EventEmitter();

    @Output()
    public toggle = new EventEmitter();

    @ViewChild('chatHistory') private chatHistoryRef: ElementRef;

    public hideChat: boolean = false;

    public chatMessage: string;

    constructor() {}

    ngAfterViewChecked() {
        this.scrollToBottom();
    }

    public sendMessage() {
        this.send.emit(this.chatMessage);
        this.chatMessage = "";
    }

    public toggleChat() {
        this.hideChat = !this.hideChat;
        this.toggle.emit(this.hideChat);
    }

    private scrollToBottom() {
        this.chatHistoryRef.nativeElement.scrollTop = this.chatHistoryRef.nativeElement.scrollHeight;
    }
}
