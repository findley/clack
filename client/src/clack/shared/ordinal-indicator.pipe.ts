import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'ordinalIndicator'})
export class OrdinalIndicator implements PipeTransform {
    transform(value: number): string {
        let j = value % 10;
        let k = value % 100;
        if (j == 1 && k != 11) {
            return value + "<sup>st</sup>";
        }
        if (j == 2 && k != 12) {
            return value + "<sup>nd</sup>";
        }
        if (j == 3 && k != 13) {
            return value + "<sup>rd</sup>";
        }
        return value + "<sup>th</sup>";
    }
}
