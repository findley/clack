import {Directive,ElementRef,Input,Output,EventEmitter} from '@angular/core';
import * as Clipboard from 'clipboard';

@Directive({
    selector: '[clipboard]'
})
export class ClipboardDirective {
    clipboard: Clipboard;

    @Input('clipboard')
    targetElem: ElementRef;

    @Input()
    cbContent: string;

    @Output('cbOnSuccess')
    onSuccess: EventEmitter<boolean> = new EventEmitter<boolean>();

    @Output('cbOnError')
    onError: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor(private elemRef: ElementRef) {}

    ngOnInit() {
        let option: Clipboard.Options;
        option = !!this.targetElem ? { target: () => <any>this.targetElem } : {text: () => this.cbContent };
        this.clipboard = new Clipboard(this.elemRef.nativeElement, option);
        this.clipboard.on('success', () => this.onSuccess.emit(true));
        this.clipboard.on('error', () => this.onError.emit(true));
    }

    ngOnDestroy() {
        if (this.clipboard) {
            this.clipboard.destroy();
        }
    }
}
