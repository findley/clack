import {Component, EventEmitter, Input, Output, HostListener, OnInit} from '@angular/core';

@Component({
    selector: 'play',
    templateUrl: './play.component.html',
    styleUrls: ['./play.component.css']
})
export class PlayComponent implements OnInit {
    @Output()
    public go = new EventEmitter();

    @Input()
    public hideInput: boolean = false;

    public name: string;

    public submit() {
        this.go.emit(this.name);
        localStorage.setItem('name', this.name);
    }

    public ngOnInit() {
        let name = localStorage.getItem('name');
        if (name) {
            this.name = name;
        }
    }

    @HostListener('window:keydown', ['$event'])
    public keyHandler(event: KeyboardEvent) {
        if (event.ctrlKey && event.keyCode == 13) {
            event.preventDefault();
            this.submit();
        }
    }
}
