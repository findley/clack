import {NgModule}                               from '@angular/core'
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {RouterModule}                           from "@angular/router";
import {FormsModule}                            from "@angular/forms";
import {BrowserModule}                          from "@angular/platform-browser";
import {HttpModule}                             from "@angular/http";

import {rootRouterConfig}    from "clack/clack.routes";
import {ClackComponent}      from "clack/clack.component";
import {GameComponent}       from 'clack/game/game.component';
import {SoloComponent}       from 'clack/solo/solo.component';
import {RoomComponent}       from 'clack/room/room.component';
import {PlayerListComponent} from 'clack/room/player-list/player-list.component';
import {CountdownComponent}  from 'clack/shared/countdown/countdown.component';
import {ChatComponent}       from 'clack/shared/chat/chat.component';
import {RaceTrackComponent}  from 'clack/shared/race-track/race-track.component';
import {RacerComponent}      from 'clack/shared/race-track/racer/racer.component';
import {SentenceComponent}   from 'clack/shared/sentence/sentence.component';
import {WordComponent}       from 'clack/shared/sentence/word/word.component';
import {LetterComponent}     from 'clack/shared/sentence/word/letter/letter.component';
import {GameService}         from 'clack/services/game.service';
import {RoomService}         from 'clack/services/room.service';
import {OrdinalIndicator}    from 'clack/shared/ordinal-indicator.pipe';
import {PlayComponent}       from 'clack/shared/play/play.component';
import {CopyitComponent}     from 'clack/shared/copyit/copyit.component';
import {ClipboardDirective } from 'clack/shared/clipboard.directive';

@NgModule({
  declarations: [
      ClackComponent,
      GameComponent,
      SoloComponent,
      RoomComponent,
      ChatComponent,
      CountdownComponent,
      RaceTrackComponent,
      RacerComponent,
      SentenceComponent,
      WordComponent,
      LetterComponent,
      OrdinalIndicator,
      PlayComponent,
      CopyitComponent,
      ClipboardDirective,
      PlayerListComponent
  ],
  imports: [
      BrowserModule,
      FormsModule,
      HttpModule,
      RouterModule.forRoot(rootRouterConfig)
  ],
  providers: [
      GameService,
      RoomService,
      {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [
      ClackComponent
  ]
})
export class ClackModule {

}
