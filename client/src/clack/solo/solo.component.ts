import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {GameService} from 'clack/services/game.service';
import * as Clack from 'clack/shared/clack.interfaces';
import {GameMode} from 'clack/shared/sentence/sentence.component';
import {Progress} from 'clack/models/progress.interface';
import {Result} from 'clack/models/result.interface';
@Component({
    selector: 'solo',
    templateUrl: './solo.component.html',
    styleUrls: ['./solo.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SoloComponent implements OnInit {
    public GameState = Clack.GameState;
    public GameMode = GameMode;
    public quote: Clack.Quote;
    public me: Clack.Racer;
    public gameState = Clack.GameState.Waiting;
    public wpm: number;
    public result: Result;

    constructor(private gameService: GameService, private cd: ChangeDetectorRef) {
        this.me = {
            id: 0,
            name: name,
            progress: 0,
            wpm: 0,
            place: null,
            connected: true
        }
        this.quote = {};
        this.wpm = 0;
    }

    public ngOnInit(): any {
        this.play()
    }

    public done(result: Result) {
        this.gameState = Clack.GameState.Finished;
        this.result = result;
        this.cd.markForCheck();
    }

    public play() {
        if (this.gameState != Clack.GameState.Finished && this.gameState != Clack.GameState.Waiting) return;

        this.me.id = 1;
        this.me.progress = 0;
        this.me.wpm = 0;
        this.me.place = null;
        this.wpm = 0;

        this.gameService.getQuote()
        .subscribe(quote => {
            this.quote.content = "";
            this.quote = quote;
            this.gameState = Clack.GameState.Starting;
            this.gameState = Clack.GameState.Started;
            this.cd.markForCheck();
        }, error => {
            console.log(error);
        })
    }

    public updateMyProgress(progress: Progress) {
        this.me.progress = progress.percentage;
        this.wpm = Math.round(progress.wpm);
        if (isNaN(this.wpm)) this.wpm = 0;
        this.cd.markForCheck();
    }
}
