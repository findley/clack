import {Component, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy} from '@angular/core';
import {GameService} from 'clack/services/game.service';
import Game from 'clack/models/game.model';
import * as Clack from 'clack/shared/clack.interfaces';
import {GameMode} from 'clack/shared/sentence/sentence.component';
import {Progress} from 'clack/models/progress.interface';

@Component({
    selector: 'game',
    templateUrl: './game.component.html',
    styleUrls: ['./game.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GameComponent implements OnDestroy {
    public GameState = Clack.GameState;
    public GameMode = GameMode;

    public game: Game;

    constructor(private gameService: GameService, private cd: ChangeDetectorRef) {}

    public done() {
        this.game.progress = 1;
    }

    public updateMyProgress(progress: Progress) {
        this.game.progress = progress.percentage;
    }

    public queue(name) {
        if (this.game) {
            this.game.close();
        }
        if (!this.game || this.game.done) {
            this.game = this.gameService.queue(name);
            this.game.subscribe(() => {
                this.cd.markForCheck();
            });
        }
    }

    public ngOnDestroy(): any {
        if (this.game) {
            this.game.close();
        }
    }
}
