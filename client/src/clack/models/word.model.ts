import Letter from 'clack/models/letter.model';

export default class Word {
    letters: Array<Letter>;
    position: number;
    letterCount: number;

    constructor(word: string) {
        let characters: Array<string> = word.split('');
        this.letterCount = word.length;
        this.position = 0;
        this.letters = [];
        for (let i = 0; i < characters.length; i++) {
            let character: string = characters[i];
            this.letters.push(new Letter(character));
        }
    }

    next(): boolean {
        if (this.isAtEnd()) return false;

        this.position++;
        this.current().makeActive();
        return true;
    }

    previous(): boolean {
        if (this.isAtStart()) return false;

        this.current().makeWaiting();
        this.position--;
        this.current().makeActive();
        return true;
    }

    current(): Letter {
        return this.letters[this.position];
    }

    first(): Letter {
        return this.letters[0];
    }

    last(): Letter {
        return this.letters[this.letters.length - 1];
    }

    isAtEnd(): boolean {
        return this.position == this.letters.length - 1;
    }

    isAtStart(): boolean {
        return this.position == 0;
    }

    makeActive() {
        this.letters[0].makeActive();
    }
}
