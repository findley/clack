import Word from 'clack/models/word.model';
import * as Clack from 'clack/shared/clack.interfaces';
import {Progress} from 'clack/models/progress.interface';
import {Result} from 'clack/models/result.interface';

export default class Sentence {
    public words: Array<Word>;
    public position: number;
    public letterCount: number;

    constructor(sentence: string) {
        this.letterCount = sentence.length;
        let wordStrings: Array<string> = sentence.split(' ');
        this.position = 0;
        this.words = [];
        for (let i = 0; i < wordStrings.length - 1; i++) {
            let word: string = wordStrings[i] + ' ';
            this.words.push(new Word(word));
        }
        this.words.push(new Word(wordStrings[wordStrings.length - 1]));
    }

    public progress(): Progress {
        let lettersCompleted = 0;
        for (let i = 0; i < this.position; i++) {
            lettersCompleted += this.words[i].letterCount;
        }
        lettersCompleted += this.current().position;
        return {
            percentage: lettersCompleted / this.letterCount,
            wpm: (lettersCompleted/5) / ((Date.now() - this.first().first().timeTyped.getTime()) / 60000)
        };
    }

    public result(): Result {
        let total = 0;
        let correct = 0;
        let incorrect = 0;
        let corrected = 0;

        for (let i = 0; i < this.words.length; i++) {
            for (let j = 0; j < this.words[i].letters.length; j++) {
                let letter = this.words[i].letters[j];
                if (letter.isCorrect()) correct++;
                if (letter.isIncorrect()) incorrect++;
                if (letter.isCorrected()) corrected++;
                total++;
            }
        }

        return {
            accuracy: (correct + corrected) / total,
            realAccuracy: (correct) / total
        };
    }

    next(): boolean {
        if (this.isAtEnd()) return false;

        if (this.current().isAtEnd()) {
            this.position++;
            this.current().makeActive();
        } else {
            this.current().next();
        }
        return true;
    }

    previous(): boolean {
        if (this.isAtStart()) return false;

        if (this.current().isAtStart()) {
            this.current().current().makeWaiting();
            this.position--;
            this.current().current().makeActive();
        } else {
            this.current().previous();
        }
        return true;
    }

    current(): Word {
        return this.words[this.position];
    }

    first(): Word {
        return this.words[0];
    }

    last(): Word {
        return this.words[this.words.length - 1];
    }

    isAtEnd(): boolean {
        return this.position == this.words.length - 1 && this.current().isAtEnd();
    }

    isAtStart(): boolean {
        return this.position == 0 && this.current().isAtStart();
    }
}
