export interface Racer {
    name: string;
    progress: number;
}
