export interface Result {
    accuracy: number;
    realAccuracy: number;
}
