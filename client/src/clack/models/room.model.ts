import {EventEmitter} from '@angular/core';
import * as Clack from 'clack/shared/clack.interfaces';
import Game from 'clack/models/game.model';
import Connection from 'clack/models/connection.model';

export default class Room extends EventEmitter<any> {
    private _connection: Connection;
    private _roomId: string;
    private _me: number;
    private _host: number;
    private _players: Array<Clack.Racer>;
    private _chatHistory: Array<Clack.ChatMessage>;
    private _game: Game;
    private _activeGame: Game;
    private _inGame: boolean;
    private _mePlayer: Clack.Racer;

    get chatHistory(): Array<Clack.ChatMessage> {
        return this._chatHistory;
    }

    get roomId(): string {
        return this._roomId;
    }

    get players(): Array<Clack.Racer> {
        return this._players;
    }

    get me(): number {
        return this._me;
    }

    get game(): Game {
        return this._game;
    }

    get inGame(): boolean {
        return this._inGame;
    }

    get mePlayer(): Clack.Racer {
        return this._mePlayer;
    }

    get host(): number {
        return this._host;
    }

    constructor(connection: Connection) {
        super();
        this._connection = connection;
        this._me = 0;
        this._players = [];
        this._chatHistory = [];
        this.listen();
        this.handleDisconnect();
        this._activeGame = new Game(connection);
        this._game = this._activeGame;
    }

    public close() {
        this._game.close();
        this._connection.close();
    }

    public amIHost(): boolean {
        return this._host == this._me;
    }

    public sendChat(message: string) {
        if (!this._connection) return;

        let msg: Clack.ChatMessage = {
            messageType: Clack.MessageType.gRoomChat,
            sender: "",
            message: message
        }

        this._connection.send(JSON.stringify(msg));
    }

    public readyUp() {
        if (!this._connection) return;
        let msg: Clack.PlayerReadyMessage = {
            messageType: Clack.MessageType.cPlayerReady
        }
        this._connection.send(JSON.stringify(msg));
        for (let player of this._players) {
            if (player.id == this._me) {
                player.ready = true;
            }
        }
    }

    public startRace() {
        if (!this._connection) return;
        if (this._me != this._host) return;
        let msg: Clack.StartRaceMessage = {
            messageType: Clack.MessageType.cStartRace
        }
        this._connection.send(JSON.stringify(msg));
    }

    public endRace() {
        if (!this._connection) return;
        if (this.me != this._host) return;
        let msg: Clack.EndRaceMessage = {
            messageType: Clack.MessageType.cEndRace
        }
        this._connection.send(JSON.stringify(msg));
    }

    private listen() {
        this._connection.onMessage.subscribe(message => {
            let m: Clack.Message = JSON.parse(message.data);
            switch(m.messageType) {
                case Clack.MessageType.sRoomJoin:
                    let roomJoinMessage = m as Clack.RoomJoinMessage;
                    this.handleRoomJoinMessage(roomJoinMessage);
                    break;
                case Clack.MessageType.sRoomStatus:
                    let roomStatusMessage = m as Clack.RoomStatusMessage;
                    this.handleRoomUpdateMessage(roomStatusMessage);
                    break;
                case Clack.MessageType.gRoomChat:
                    let chatMessage = m as Clack.ChatMessage;
                    this.handleChatMessage(chatMessage);
                    break;
                case Clack.MessageType.sRaceJoin:
                    console.log('Flipping game');
                    this._game = this._activeGame;
                    this.emit();
            }
        });
    }

    private handleDisconnect() {
        this._connection.onClose.subscribe(() => {
            this._chatHistory.push({
                messageType: Clack.MessageType.gRoomChat,
                sender: '',
                message: 'Disconnected from room.',
                system: true
            });
        });
        this.emit();
    }

    private handleRoomJoinMessage(message: Clack.RoomJoinMessage) {
        this._roomId = message.roomId;
        this._me = message.you;
        this._host = message.host;
        this._players = message.players;
        this._inGame = message.inGame;
        this.assignMePlayer();

        this.emit();
    }

    private handleRoomUpdateMessage(message: Clack.RoomStatusMessage) {
        if (this._inGame && !message.inGame) {
            // Game ended
            console.log('game ended.');
            this._activeGame = new Game(this._connection);
        }
        this._host = message.host;
        this._inGame = message.inGame;
        for (let updatedPlayer of message.players) {
            this.updateOrAddPlayer(updatedPlayer)
        }
        this.removeMissingPlayers(message.players);
        this.emit();
    }

    private updateOrAddPlayer(updatedPlayer: Clack.Racer) {
        for (let player of this._players) {
            if (player.id == updatedPlayer.id) {
                Object.assign(player, updatedPlayer);
                return;
            }
        }
        this._players.push(updatedPlayer);
    }

    private removeMissingPlayers(players: Array<Clack.Racer>) {
        let toRemove: Array<number> = [];
        for (let i = this._players.length-1; i >= 0; i--) {
            let found = false;
            for (let updatedPlayer of players) {
                if (this._players[i].id == updatedPlayer.id) {
                    found = true;
                }
            }
            if (!found) {
                this._players.splice(i, 1);
            }
        }
    }

    private assignMePlayer() {
        for (let player of this._players) {
            if (player.id == this._me) {
                this._mePlayer = player;
            }
        }
    }

    private handleChatMessage(message: Clack.ChatMessage) {
        this._chatHistory.push(message);
        this.emit();
    }
}
