export interface Progress {
    percentage: number;
    wpm: number;
}
