import {EventEmitter} from '@angular/core';
import * as Clack from 'clack/shared/clack.interfaces';
import Connection from 'clack/models/connection.model';

export default class Game extends EventEmitter<any> {
    private static readonly TICKS_PER_SECOND = 3;
    private _connection: Connection;
    private _lastProgress: number;
    private _race: Clack.Race;
    private _quote: Clack.Quote;
    private _startsIn: number;
    private _me: number;
    private _progress: number;
    private _updateInterval: any;
    private _gameId: number;

    constructor(connection: Connection) {
        super();
        this._connection = connection;
        this._quote = {};
        this._me = 0;
        this._progress = 0;
        this.listen();
    }

    public close() {
        this._connection.close();
        clearInterval(this._updateInterval);
    }

    get clackRace(): Clack.Race {
        return this._race;
    }

    get quote(): Clack.Quote {
        return this._quote;
    }

    get me(): number {
        return this._me;
    }

    get startsIn(): number {
        return this._startsIn;
    }

    get progress(): number {
        return this._progress;
    }

    set progress(p: number) {
        this._progress = p;
    }

    get done(): boolean {
        return this._progress >= 1;
    }

    get state(): Clack.GameState {
        if (this._race) {
            return this._race.state;
        }
        return Clack.GameState.Waiting;
    }

    private sendProgress() {
        if (!this._connection) return;
        if (this._lastProgress !== null && this._progress == this._lastProgress) return;

        let msg: Clack.RacerUpdateMessage = {
            messageType: Clack.MessageType.cRacerUpdate,
            progress: this._progress
        }

        this._connection.send(JSON.stringify(msg));
        this._lastProgress = this._progress;
    }

    private listen() {
        this._connection.onMessage.takeWhile(() => {
            if (!(!this._race || this._race.state != Clack.GameState.Finished)) {
                console.log('Stop taking game messages');
            }
            return !this._race || this._race.state != Clack.GameState.Finished;
        }).subscribe(message => {
            let m: Clack.Message = JSON.parse(message.data);
            switch(m.messageType) {
                case Clack.MessageType.sRaceJoin:
                    let raceJoinMessage = m as Clack.RaceJoinMessage;
                    this.handleRaceJoinMessage(raceJoinMessage);
                    break;
                case Clack.MessageType.sRaceUpdate:
                    let raceUpdateMessage = m as Clack.RaceUpdateMessage;
                    this.handleRaceUpdateMessage(raceUpdateMessage);
                    break;
            }
        });
    }

    private handleRaceJoinMessage(message: Clack.RaceJoinMessage) {
        this._race = message.race;
        this._quote = message.quote;
        this._startsIn = message.startsIn;
        this._gameId = message.raceId;
        if (message.you) {
            this._me = message.you;
        }
        setTimeout(() => {
            this._race.state = Clack.GameState.Started
            if (message.you) {
                this._updateInterval = setInterval(() => {
                    this.sendProgress();
                    if (this._progress >= 1) {
                        clearInterval(this._updateInterval);
                    }
                }, (1 / Game.TICKS_PER_SECOND) * 1000);
            }
        }, message.startsIn);

        this.emit();
    }

    private handleRaceUpdateMessage(message: Clack.RaceUpdateMessage) {
        if (this._gameId != message.raceId) return;
        if (!this._race || message.race.state == Clack.GameState.Loading || message.race.state == Clack.GameState.Starting) {
            this._race = message.race;
        } else {
            this._race.state = message.race.state;
            for (let racer of this._race.racers) {
                for (let racerUpdated of message.race.racers) {
                    if (racer.id == racerUpdated.id) {
                        racer.place = racerUpdated.place;
                        racer.connected = racerUpdated.connected;
                        racer.wpm = racerUpdated.wpm;
                        if (racer.id != this._me) {
                            racer.progress = racerUpdated.progress;
                        }
                        break;
                    }
                }
            }
        }
        this.emit();
    }
}
