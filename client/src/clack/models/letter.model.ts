import {EventEmitter} from '@angular/core';

export enum State {
    Waiting = 1,
    Active,
    Correct,
    Incorrect,
    Corrected
}

export default class Letter extends EventEmitter<any> {
    state: State;
    character: string;
    timeTyped: Date;
    missed: boolean = false;
    mistake: boolean = false;

    constructor(character: string) {
        super();
        this.state = State.Waiting;
        this.character = character;
    }

    makeActive() {
        this.state = State.Active;
        this.emit();
    }

    makeWaiting() {
        this.state = State.Waiting;
        this.emit();
    }

    markCorrect() {
        if (this.missed) {
            this.state = State.Corrected;
        } else {
            this.state = State.Correct;
        }
        this.timeTyped = new Date();
        this.emit();
    }

    markIncorrect() {
        this.state = State.Incorrect;
        this.timeTyped = new Date();
        this.missed = true;
        this.emit();
    }

    public isCorrect() {
        return this.state == State.Correct;
    }

    public isIncorrect() {
        return this.state == State.Incorrect;
    }

    public isCorrected() {
        return this.state == State.Corrected;
    }

    flashMistake() {
        this.mistake = true;
        setTimeout(() => {
            this.mistake = false;
            this.emit();
        }, 333);
        this.emit();
    }
}
