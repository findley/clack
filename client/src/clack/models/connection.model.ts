import {EventEmitter} from '@angular/core';

export default class Connection {
    public onMessage = new EventEmitter<MessageEvent>();
    public onClose = new EventEmitter();

    private _ws: WebSocket;

    constructor(ws: WebSocket) {
        this._ws = ws;
        ws.onmessage = (message: MessageEvent) => {
            this.onMessage.emit(message);
        };

        ws.onclose = () => {
            this.onClose.emit();
        };
    }

    public get ws(): WebSocket {
        return this._ws;
    }

    public send(message: any) {
        if (this._ws.readyState != this._ws.OPEN) return;
        this._ws.send(message);
    }

    public close() {
        this._ws.close();
    }

    public static fromUrl(url: string): Connection {
        let socket = new WebSocket(url)
        return new Connection(socket);
    }
}
