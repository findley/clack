import {Component, Input} from '@angular/core';
import * as Clack from 'clack/shared/clack.interfaces';

@Component({
    selector: 'player-list',
    templateUrl: './player-list.component.html',
    styleUrls: ['./player-list.component.css']
})
export class PlayerListComponent {
    @Input()
    public players: Array<Clack.Racer>;

    @Input()
    public host: number;

    @Input()
    public me: number;
}
