import {Location} from '@angular/common';
import {Component, ChangeDetectionStrategy, ChangeDetectorRef, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {RoomService} from 'clack/services/room.service';
import * as Clack from 'clack/shared/clack.interfaces';
import Room from 'clack/models/room.model';
import {GameMode} from 'clack/shared/sentence/sentence.component';
import {Progress} from 'clack/models/progress.interface';

@Component({
    selector: 'room',
    templateUrl: './room.component.html',
    styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnDestroy, OnInit {
    public GameState = Clack.GameState;
    public GameMode = GameMode;

    public room: Room;

    public hideChat: boolean = false;

    public host: string;

    private _roomId: string;

    get roomId(): string {
        return this._roomId;
    }

    constructor(private location: Location, private roomService: RoomService, private cd: ChangeDetectorRef, private route: ActivatedRoute) {}

    public ngOnInit() {
        this.host = window.location.protocol + '//' + window.location.host
        this.route.queryParams.subscribe(params => {
            this._roomId = params['id'];
        });
    }

    public ngOnDestroy(): any {
        if (this.room) {
            this.room.close();
        }
    }

    private connect(name) {
        this.room = this.roomService.join(name, this._roomId);
        this.room.subscribe(() => {
            if (!this._roomId && this.room.roomId || this._roomId != this.room.roomId) {
                this._roomId = this.room.roomId;
                this.location.go('/room?id='+this.room.roomId);
            }
        });
    }

    public done() {
        this.room.game.progress = 1;
    }

    public updateMyProgress(progress: Progress) {
        this.room.game.progress = progress.percentage;
    }

    public replay(name) {
    }
}
