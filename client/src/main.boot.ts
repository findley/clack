import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {ClackModule} from 'clack/clack.module';
import {enableProdMode} from '@angular/core';

enableProdMode();
platformBrowserDynamic().bootstrapModule(ClackModule)
  .catch(err => console.error(err));
